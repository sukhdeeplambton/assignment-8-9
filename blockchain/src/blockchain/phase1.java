package blockchain;

import java.util.Date;

public class phase1 {
	public String hash;
	public String previousHash;
	private String data;
	private long timeStamp;
	
	public  phase1 (String data,String previousHash ) {
		this.setData(data);
		this.previousHash = previousHash;
		this.setTimeStamp(new Date().getTime());
	}

	public long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

}
